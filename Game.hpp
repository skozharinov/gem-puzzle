#ifndef GEMPUZZLE_GAME_HPP
#define GEMPUZZLE_GAME_HPP

#include <algorithm>
#include <array>
#include <cstdint>
#include <iterator>
#include <numeric>

namespace GemPuzzle::Game {
using IndexT = std::uint8_t;
using CoordsT = std::pair<IndexT, IndexT>;
using SumT = std::uint16_t;

template <IndexT Rows, IndexT Cols> class State;

template <IndexT Rows, IndexT Cols> class Hash;

enum class Move : std::uint8_t {
  Up,
  Down,
  Left,
  Right,
  None,
};

constexpr Move ReverseMove(Move change);

template <IndexT Rows, IndexT Cols> class State {
public:
  static const IndexT CellsCount = Rows * Cols;

  friend class Hash<Rows, Cols>;

  constexpr State() : cells(Indices()), zeroIndex(CellsCount - 1) {
    std::rotate(cells.begin(), cells.begin() + 1, cells.end());
  }

  template <std::input_iterator InputIt>
  constexpr explicit State(InputIt inputIt) : cells() {
    // TODO: rewrite using ranges
    // TODO: check for errors
    std::copy_n(inputIt, CellsCount, cells.begin());
    zeroIndex = std::distance(cells.cbegin(),
                              std::find(cells.cbegin(), cells.cend(), 0));
  }

  [[nodiscard]] constexpr IndexT GetTile(CoordsT coords) const {
    return GetCell(GetTileIndex(coords));
  }

  [[nodiscard]] constexpr SumT GetDist() const {
    const auto indices = Indices();

    return std::transform_reduce(
        cells.cbegin(), cells.cend(), indices.cbegin(), 0, std::plus<>(),
        [this](auto cell, auto index) {
          bool isNotZero = index != zeroIndex;
          SumT dist = GetCoordsDiff(GetCellCoords(index),
                                    GetCellCoords(GetTargetIndex(cell)));

          return isNotZero * dist;
        });
  }

  [[nodiscard]] constexpr SumT GetLinearConflicts() const {
    const auto indices = Indices();
    std::array<std::pair<bool, bool>, CellsCount> inRowOrCol;

    std::transform(cells.cbegin(), cells.cend(), indices.cbegin(),
                   inRowOrCol.begin(), [](auto cell, auto index) {
                     auto [row, col] = GetCellCoords(index);
                     auto [targetRow, targetCol] =
                         GetCellCoords(GetTargetIndex(cell));

                     return std::make_pair(row == targetRow, col == targetCol);
                   });

    return std::transform_reduce(
        indices.cbegin(), indices.cend(), 0, std::plus<>(),
        [this, inRowOrCol](auto index) {
          SumT result = 0;

          if (inRowOrCol[index].first) {
            for (IndexT test = index + 1; test < (index / Cols + 1) * Cols;
                 ++test) {
              result += (test != zeroIndex) * (inRowOrCol[test].first) *
                        (GetCell(test) < GetCell(index));
            }
          }

          if (inRowOrCol[index].second) {
            for (IndexT test = index + Cols; test < CellsCount; test += Cols) {
              result += (test != zeroIndex) * (inRowOrCol[test].second) *
                        (GetCell(test) < GetCell(index));
            }
          }

          return result;
        });
  }

  [[nodiscard]] constexpr State GetStateAfterMove(Move state) const {
    State newState(*this);

    auto oldIndex = zeroIndex;
    auto newIndex = GetNewIndex(zeroIndex, state);

    std::swap(newState.cells[oldIndex], newState.cells[newIndex]);
    newState.zeroIndex = newIndex;

    return newState;
  }

  constexpr bool operator==(const State& other) const {
    return cells == other.cells;
  }

protected:
  static constexpr IndexT GetIndexDiff(IndexT coord, IndexT fromCoord) {
    return (coord < fromCoord) * (fromCoord - coord) +
           (coord > fromCoord) * (coord - fromCoord);
  }

  static constexpr CoordsT GetCellCoords(IndexT index) {
    return {index / Cols, index % Cols};
  }

  static constexpr IndexT GetTileIndex(CoordsT coords) {
    return coords.first * Cols + coords.second;
  }

  static constexpr IndexT GetCoordsDiff(CoordsT coords, CoordsT fromCoords) {
    return GetIndexDiff(coords.first, fromCoords.first) +
           GetIndexDiff(coords.second, fromCoords.second);
  }

  static constexpr IndexT GetTargetIndex(IndexT index) {
    return (index + CellsCount - 1) % CellsCount;
  }

  static constexpr IndexT GetNewIndex(IndexT index, Move change) {
    auto [row, col] = GetCellCoords(index);

    bool isMinRow = row == 0, isMaxRow = row == Rows - 1;
    bool isMinCol = col == 0, isMaxCol = col == Cols - 1;

    return (change == Move::Down) * (index + Cols * (not isMaxRow)) +
           (change == Move::Up) * (index - Cols * (not isMinRow)) +
           (change == Move::Right) * (index + 1 * (not isMaxCol)) +
           (change == Move::Left) * (index - 1 * (not isMinCol));
  }

  static constexpr std::array<IndexT, CellsCount> Indices() {
    std::array<IndexT, CellsCount> indices{};
    std::iota(indices.begin(), indices.end(), 0);
    return indices;
  }

  [[nodiscard]] constexpr IndexT GetCell(IndexT index) const {
    return cells[index];
  }

private:
  std::array<IndexT, CellsCount> cells;
  IndexT zeroIndex;
};

template <IndexT Rows, IndexT Cols> class Hash {
public:
  constexpr Hash() : powers() {
    std::generate(powers.begin(), powers.end(), [power = 1]() mutable {
      auto result = power;
      power = (power * Prime) % Modulo;
      return result;
    });
  }

  constexpr std::size_t operator()(const State<Rows, Cols>& state) const {
    return std::transform_reduce(
        powers.cbegin(), powers.cend(), state.cells.cbegin(), std::size_t{},
        [](auto cell1, auto cell2) { return (cell1 + cell2) % Modulo; },
        [](auto power, auto cell) { return power * cell % Modulo; });
  }

private:
  static const std::size_t Prime = 31;
  static const std::size_t Modulo = 1e9 + 7;

  std::array<std::size_t, State<Rows, Cols>::CellsCount> powers;
};

static constexpr Move ReverseMove(Move change) {
  switch (change) {
  case Move::Up:
    return Move::Down;

  case Move::Down:
    return Move::Up;

  case Move::Left:
    return Move::Right;

  case Move::Right:
    return Move::Left;

  default:
    return Move::None;
  }
}
} // namespace GemPuzzle::Game

#endif // GEMPUZZLE_GAME_HPP

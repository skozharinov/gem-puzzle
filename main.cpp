#include "Solver.hpp"

#include <iostream>
#include <random>
#include <chrono>
#include <ranges>

bool IsPositiveInteger(std::string_view str) {
  return str.find_first_not_of("0123456789") == std::string_view::npos;
}

std::size_t ReadUserChoice(std::initializer_list<std::string_view> options) {
  using namespace std;

  for (size_t i = 0; auto option : options)
    cout << ++i << ") " << option << "\n";

  cout << "? ";

  string choice;
  while (std::cin >> choice) {
    if (IsPositiveInteger(choice)) {
      size_t result = stoul(choice);

      if (result > 0 and result <= options.size())
        return result;
    }

    cout << "? ";
  }

  return 0;
}

std::string_view GetHumanReadableMove(GemPuzzle::Game::Move move) {
  using namespace GemPuzzle;

  switch (move) {
  case Game::Move::Up:
    return "U";

  case Game::Move::Down:
    return "D";

  case Game::Move::Left:
    return "L";

  case Game::Move::Right:
    return "R";

  default:
    return "";
  }
}

int main(int argc, char** argv) {
  using namespace std;
  using namespace GemPuzzle;

  const Game::IndexT Rows = 4, Cols = 4;

  array<Game::IndexT, Game::State<Rows, Cols>::CellsCount> cells{};

  auto choice = ReadUserChoice({"Randomize", "Input"});

  if (choice == 1) {
    iota(cells.begin(), cells.end(), 0);

    random_device device;
    mt19937 generator(device());

    shuffle(cells.begin(), cells.end(), generator);
  } else if (choice == 2) {
    cout << "Please type in " << cells.size() << " numbers: ";
    copy_n(istream_iterator<uint32_t>(cin), cells.size(), cells.begin());
  } else {
    return 0;
  }

  Game::State<Rows, Cols> state(cells.cbegin());

  for (Game::IndexT row = 0; row < Rows; ++row) {
    for (Game::IndexT col = 0; col < Cols; ++col) {
      uint32_t tile = state.GetTile({row, col});
      cout << tile << "\t";
    }

    cout << "\n";
  }

  cout << endl;

  auto start = chrono::high_resolution_clock::now();

  auto path = Solver::DoAStar(state);

  auto end = chrono::high_resolution_clock::now();

  auto time = chrono::duration_cast<chrono::seconds>(end - start);

  const size_t MovesOnOneLine = 5;

  if (path.empty()) {
    cout << "No solution has been discovered in " << time;
  } else {
    cout << "Discovered a solution of " << path.size() << " moves in " << time
         << "\n";

    ranges::copy(path | views::transform(GetHumanReadableMove),
                 ostream_iterator<string_view>(cout));
  }

  cout << endl;
}

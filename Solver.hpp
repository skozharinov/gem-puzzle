#ifndef GEMPUZZLE_SOLVER_HPP
#define GEMPUZZLE_SOLVER_HPP

#include "Game.hpp"

#include <queue>
#include <unordered_map>
#include <vector>

namespace GemPuzzle::Solver {
template <Game::IndexT Rows, Game::IndexT Cols> class Node {
public:
  constexpr explicit Node(Game::State<Rows, Cols> state)
      : Node(state, 0, Game::Move::None) {}

  constexpr Node(Game::State<Rows, Cols> state, Game::SumT cost,
                 Game::Move lastMove)
      : state(state),
        estimate(state.GetDist() + 2 * state.GetLinearConflicts()),
        cost(cost), lastMove(lastMove) {}

  [[nodiscard]] constexpr Game::State<Rows, Cols> GetState() const {
    return state;
  }

  [[nodiscard]] constexpr Game::SumT GetSum() const { return estimate + cost; }

  [[nodiscard]] constexpr Game::SumT GetEstimate() const { return estimate; }

  [[nodiscard]] constexpr Game::Move GetLastMove() const { return lastMove; }

  constexpr bool operator<(const Node& other) const {
    return GetSum() < other.GetSum();
  }

  constexpr bool operator>(const Node& other) const {
    return GetSum() > other.GetSum();
  }

  constexpr Node operator+(Game::Move move) const {
    return {state.GetStateAfterMove(move), static_cast<Game::SumT>(cost + 1),
            move};
  }

private:
  Game::State<Rows, Cols> state;
  Game::SumT estimate;
  Game::SumT cost;
  Game::Move lastMove;
};

template <Game::IndexT Rows, Game::IndexT Cols>
std::vector<Game::Move> DoAStar(Game::State<Rows, Cols> start) {
  const Game::State<Rows, Cols> target;

  std::priority_queue<Node<Rows, Cols>, std::vector<Node<Rows, Cols>>,
                      std::greater<>>
      open;
  std::unordered_map<Game::State<Rows, Cols>, Game::Move,
                     Game::Hash<Rows, Cols>>
      closed;

  open.emplace(start);

  while (not open.empty() and open.top().GetEstimate() != 0) {
    auto node = open.top();
    open.pop();

    if (closed.contains(node.GetState()))
      continue;

    closed.emplace(node.GetState(), node.GetLastMove());

    open.push(node + Game::Move::Up);
    open.push(node + Game::Move::Down);
    open.push(node + Game::Move::Left);
    open.push(node + Game::Move::Right);
  }

  if (open.empty())
    return {};

  std::vector<Game::Move> path{Game::ReverseMove(open.top().GetLastMove())};
  auto state = target.GetStateAfterMove(path[0]);

  while (closed[state] != Game::Move::None) {
    auto reverseMove = ReverseMove(closed[state]);
    path.push_back(reverseMove);
    state = state.GetStateAfterMove(reverseMove);
  }

  std::reverse(path.begin(), path.end());
  return path;
}
} // namespace GemPuzzle::Solver

#endif // GEMPUZZLE_SOLVER_HPP
